<?php

$doc = Jfactory::getDocument();
$doc->addStyleSheet(DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'mod_k2_mosaicum' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'mod_k2_mosaicum_default.css');

//Comprobar que la carpeta de cache exista o crearla
if (!file_exists(JPATH_ROOT . '/cache/mod_k2_mosaicum')) {
    mkdir(JPATH_ROOT . '/cache/mod_k2_mosaicum', 0777, true);
}

// Importar la clase rezise-class.php
// Include_once JPATH_ROOT . '/modules/mod_k2_mosaicum/class/resize-class.php';
$size = 'M';

$template = '<div class="mosaico">';
$x = 1;
foreach ($dataItems as $item) {
    $url = JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . urlencode($item->alias), $item->catid . ':' . urlencode($item->catalias)));
    if ($x == 1) {
        $image = '/media/k2/items/cache/' . md5('Image' . $item->id) . '_M.jpg';
        $template .= '<div class="s1">'
                . '<div>'
                . '<a href="' . $url . '"><img src="' . $image . '" alt="' . $item->title . '"></a>'
//                . '<div class="deg"></div>'
                . '<a href="' . $url . '" class="mask">'
                . '<h3>' . $item->title . '</h3>'
                . '</a>'
                . '</div></div>';
        $x++;
    }else    
    if ($x == 2) {
        $image = $k2Mosaicum->setCache($item->id, 'M');
        $template .= '<div class="s3">'
                . '<div>'
                . '<a href="' . $url . '"><img src="' . $image . '" alt="' . $item->title . '"></a>'
//                . '<div class="deg"></div>'
                . '<a href="' . $url . '" class="mask">'
                . '<h3>' . $item->title . '</h3>'
                . '</a>'
                . '</div>';
        $x++;
    } else
    if ($x == 3) {
        $image = $k2Mosaicum->setCache($item->id, 'M');
        $template .= '<div>'
                . '<a href="' . $url . '"><img src="' . $image . '" alt="' . $item->title . '"></a>'
//                . '<div class="deg"></div>'
                . '<a href="' . $url . '" class="mask">'
                . '<h3>' . $item->title . '</h3>'
                . '</a>'
                . '</div>';
        $x++;
    } else
    if ($x == 4) {
        $image = $k2Mosaicum->setCache($item->id, 'M');
        $template .= '<div>'
                . '<a href="' . $url . '"><img src="' . $image . '" alt="' . $item->title . '"></a>'
//                . '<div class="deg"></div>'
                . '<a href="' . $url . '" class="mask">'
                . '<h3>' . $item->title . '</h3>'
                . '</a>'
                . '</div></div>';
        $x++;
    } else
    if ($x == 5) {
        $image = '/media/k2/items/cache/' . md5('Image' . $item->id) . '_S.jpg';
        $template .= '<div class="s2">'
                . '<div>'
                . '<img src="' . $image . '" alt="' . $item->title . '">'
                . '<div class="deg"></div>'
                . '<a href="' . $url . '" class="mask">'
                . '<h3>' . $item->title . '</h3>'
                . '<span class="icon-plus"></span>'
                . '</a>'
                . '</div>';
        $x++;
    } else
    if ($x == 6) {
        $image = '/media/k2/items/cache/' . md5('Image' . $item->id) . '_S.jpg';
        $template .= '<div>'
                . '<img src="' . $image . '" alt="' . $item->title . '">'
                . '<div class="deg"></div>'
                . '<a href="' . $url . '" class="mask">'
                . '<h3>' . $item->title . '</h3>'
                . '<span class="icon-plus"></span>'
                . '</a>'
                . '</div></div></div>';
        $x = 1;
    }
}
$template .= '</div>';
echo $template;