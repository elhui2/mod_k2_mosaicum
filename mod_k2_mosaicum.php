<?php

defined('_JEXEC') or die('Restricteted access');

require_once dirname(__FILE__) . '/helper.php';

$k2Mosaicum = new k2Mosaicum($params);
$dataItems = $k2Mosaicum->sortItems();

require(JModuleHelper::getLayoutPath('mod_k2_mosaicum'));
