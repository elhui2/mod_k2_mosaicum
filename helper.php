<?php

require_once(JPATH_SITE . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_k2' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'route.php');
require_once(JPATH_SITE . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'mod_k2_mosaicum' . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'resize-class.php');

class k2Mosaicum {

    private $params;
    private $query;

    public function __construct($params) {
        $this->params = $params;
    }

    /**
     * 
     * @return type
     */
    function sortItems() {
        $query = 'SELECT a.id AS id,a.catid AS catid,a.title AS title,a.introtext AS introtext,a.hits AS hits,a.alias AS alias,c.alias AS catalias FROM #__k2_items AS a LEFT JOIN #__k2_categories AS c ON ( a.catid = c.id ) WHERE (a.published = 1)';
        $cid = $this->params->get('category_id', null);
        if (!$this->params->get('catfilter')) {
            if (is_array($cid)) {
                if ($this->params->get('getChildren')) {
                    $itemListModel = K2Model::getInstance('Itemlist', 'K2Model');
                    $categories = $itemListModel->getCategoryTree($cid);
                    $sql = implode(',', $categories);
                    $query .= " AND a.catid IN ({$sql})";
                } else {
                    JArrayHelper::toInteger($cid);
                    $query .= " AND a.catid IN(" . implode(',', $cid) . ")";
                }
            } else {
                if ($this->params->get('getChildren')) {
                    $itemListModel = K2Model::getInstance('Itemlist', 'K2Model');
                    $categories = $itemListModel->getCategoryTree($cid);
                    $sql = implode(',', $categories);
                    $query .= " AND a.catid IN ({$sql})";
                } else {
                    $query .= " AND a.catid=" . (int) $cid;
                }
            }
        }

        if ($this->params->get('featureditems') == 0) {
            $query .= ' AND (a.featured != 0)';
        } else
        if ($this->params->get('featureditems') == 1) {
            $query .= '';
        } else
        if ($this->params->get('featureditems') == 2) {
            $query .= ' AND (a.featured = 1)';
        }


        $query .= ' ORDER BY a.' . $this->params->get('itemsOrdering') . ' DESC ' . 'LIMIT ' . $this->params->get('itemCount');
        
        // Query sampler
        // SELECT a.id AS id,a.catid AS catid,a.title AS title,a.introtext AS introtext,a.hits AS hits,a.alias AS alias,c.alias AS catalias FROM #__k2_items AS a LEFT JOIN #___k2_categories AS c ON ( a.catid = c.id ) WHERE (a.published = 1) AND (a.featured = 1) ORDER BY a.created DESC LIMIT 5
        $db = JFactory::getDBO();
        $db->setQuery($query);
        $articles = $db->loadObjectList();

        return $articles;
    }

    /**
     * 
     * @param type $dataItems
     * @return string
     */
    public function getDefault($dataItems) {
        //Comprobar que la carpeta de cache exista o crearla
        if (!file_exists(JPATH_ROOT . '/cache/mod_k2_mosaicum')) {
            mkdir(JPATH_ROOT . '/cache/mod_k2_mosaicum', 0777, true);
        }

        // Importar la clase rezise-class.php
        // Include_once JPATH_ROOT . '/modules/mod_k2_mosaicum/class/resize-class.php';
        $size = 'M';

        $template = '<div class="mosaico">';
        $x = 1;
        foreach ($dataItems as $item) {
            $url = JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . urlencode($item->alias), $item->catid . ':' . urlencode($item->catalias)));
            if ($x == 1) {
                $image = $this->setCache($item->id, 'M');
                $template .= '<div class="s3">'
                        . '<div>'
                        . '<img src="' . $image . '" alt="' . $item->title . '">'
                        . '<div class="deg"></div>'
                        . '<a href="' . $url . '" class="mask">'
                        . '<h3>' . $item->title . '</h3>'
                        . '<span class="icon-plus"></span>'
                        . '</a>'
                        . '</div>';
                $x++;
            } else
            if ($x == 2) {
                $image = $this->setCache($item->id, 'M');
                $template .= '<div>'
                        . '<img src="' . $image . '" alt="' . $item->title . '">'
                        . '<div class="deg"></div>'
                        . '<a href="' . $url . '" class="mask">'
                        . '<h3>' . $item->title . '</h3>'
                        . '<span class="icon-plus"></span>'
                        . '</a>'
                        . '</div>';
                $x++;
            } else
            if ($x == 3) {
                $image = $this->setCache($item->id, 'M');
                $template .= '<div>'
                        . '<img src="' . $image . '" alt="' . $item->title . '">'
                        . '<div class="deg"></div>'
                        . '<a href="' . $url . '" class="mask">'
                        . '<h3>' . $item->title . '</h3>'
                        . '<span class="icon-plus"></span>'
                        . '</a>'
                        . '</div></div>';
                $x++;
            } else
            if ($x == 4) {
                $image = '/media/k2/items/cache/' . md5('Image' . $item->id) . '_M.jpg';
                $template .= '<div class="row">'
                        . '<div class="s1">'
                        . '<div>'
                        . '<img src="' . $image . '" alt="' . $item->title . '">'
                        . '<div class="deg"></div>'
                        . '<a href="' . $url . '" class="mask">'
                        . '<h3>' . $item->title . '</h3>'
                        . '<span class="icon-plus"></span>'
                        . '</a>'
                        . '</div></div>';
                $x++;
            } else
            if ($x == 5) {
                $image = '/media/k2/items/cache/' . md5('Image' . $item->id) . '_S.jpg';
                $template .= '<div class="s2">'
                        . '<div>'
                        . '<img src="' . $image . '" alt="' . $item->title . '">'
                        . '<div class="deg"></div>'
                        . '<a href="' . $url . '" class="mask">'
                        . '<h3>' . $item->title . '</h3>'
                        . '<span class="icon-plus"></span>'
                        . '</a>'
                        . '</div>';
                $x++;
            } else
            if ($x == 6) {
                $image = '/media/k2/items/cache/' . md5('Image' . $item->id) . '_S.jpg';
                $template .= '<div>'
                        . '<img src="' . $image . '" alt="' . $item->title . '">'
                        . '<div class="deg"></div>'
                        . '<a href="' . $url . '" class="mask">'
                        . '<h3>' . $item->title . '</h3>'
                        . '<span class="icon-plus"></span>'
                        . '</a>'
                        . '</div></div></div>';
                $x = 1;
            }
        }
        $template .= '</div>';
        return $template;
    }

    public function issos($dataItems) {
        $template = '<div class="issos">';
        $image = $this->params->get('cdnimages');
        $sizes = array('XS', 'S', 'M');


        foreach ($dataItems as $item) {
            $image = $this->params->get('cdnimages');
            $url = JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . urlencode($item->alias), $item->catid . ':' . urlencode($item->catalias)));
            $image = '/media/k2/items/cache/' . md5('Image' . $item->id) . '_S.jpg';
            $template .= '<div class="issos-item">'
                    . '<a href="' . $url . '">'
                    . '<img src="' . $image . '" alt="' . $item->title . '"></a>'
                    . '<a href="' . $url . '">' . $item->title . '</a>'
                    . '</div>';
        }
        $template .= '</div>';
        return $template;
    }

    public function getVideos($dataItems) {
        $template = '<div class="videos-slideshow"><h1>Videos</h1>';
        foreach ($dataItems as $item) {
            $image = $this->params->get('cdnimages');
            $url = JRoute::_(K2HelperRoute::getItemRoute($item->id . ':' . urlencode($item->alias), $item->catid . ':' . urlencode($item->catalias)));
            $image = '/media/k2/items/cache/' . md5('Image' . $item->id) . '_S.jpg';
            $template .= '<div class="videos-ssitem">'
                    . '<a href="' . $url . '">'
                    . '<img src="' . $image . '" alt="' . $item->title . '"></a>'
                    . '<a href="' . $url . '">' . $item->title . '</a>'
                    . '</div>';
        }
        $template .= '</div>';
        return $template;
    }

    public function setCache($itemId, $size) {

        //Comprobar que la carpeta de cache exista o crearla
        if (!file_exists(JPATH_ROOT . '/cache/mod_k2_mosaicum')) {
            mkdir(JPATH_ROOT . '/cache/mod_k2_mosaicum', 0777, true);
        }

        //Comprobar que las imagenes existan o crearlas
        if (file_exists(JPATH_ROOT . '/media/k2/items/cache/' . md5('Image' . $itemId) . '_' . $size . '.jpg') && file_exists(JPATH_ROOT . '/cache/mod_k2_mosaicum/' . md5('Image' . $itemId) . '_' . $size . '.jpg')) {
            $url = '/cache/mod_k2_mosaicum/' . md5('Image' . $itemId) . '_' . $size . '.jpg';
        } else
        if (file_exists(JPATH_ROOT . '/media/k2/items/cache/' . md5('Image' . $itemId) . '_' . $size . '.jpg') && !file_exists(JPATH_ROOT . '/cache/mod_k2_mosaicum/' . md5('Image' . $itemId) . '_' . $size . '.jpg')) {

            $resize = new resize(JPATH_ROOT . '/media/k2/items/cache/' . md5('Image' . $itemId) . '_' . $size . '.jpg');
            $resize->resizeImage(270, 270, $option = 'crop');
            $resize->saveImage(JPATH_ROOT . '/cache/mod_k2_mosaicum/' . md5('Image' . $itemId) . '_' . $size . '.jpg', 75);

            $url = '/cache/mod_k2_mosaicum/' . md5('Image' . $itemId) . '_' . $size . '.jpg';
        }

        return $url;
    }

}
